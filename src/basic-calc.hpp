#pragma once

#include <gtkmm.h>

class BasicCalculator : public Gtk::Window {
public:
    BasicCalculator();

private:
    double operand;
    double result;
    bool   is_1st_operand_ready;
    bool   is_2nd_operand_ready;
    bool   is_1st_operand_negative;
    bool   is_1st_operand_in_precision;
    bool   is_2nd_operand_in_precision;

    Glib::RefPtr<Gtk::Builder> window_calculator;
    Glib::RefPtr<Gtk::Builder> window_shortcuts;
    Glib::RefPtr<Gtk::Builder> window_header;
    Glib::RefPtr<Gtk::Builder> window_about;
    Glib::RefPtr<Gdk::Pixbuf>  window_icon;

    /*
     * Basic Calculator GUI components
     * To be called when constructor runs
     */
    void prepare_window        ();
    void prepare_number_buttons(Gtk::Label *);
    void prepare_action_buttons(Gtk::Label *);
    void prepare_theme         ();

    /* Signal events */
    bool on_window_state_changed     (GdkEventWindowState *, Gtk::Box *);
    void on_button_clicked           (Gtk::Button *, Gtk::Label *);
    bool on_key_pressed              (GdkEventKey *, Gtk::Label *);
    void on_action_about             ();
    void on_about_dialog_response    (int, Gtk::AboutDialog *);
    void on_action_keyboard_shortcuts();

    /*
     * Manipulate display and get operand operation
     *
     * Pre-action
     * Verify input before processing main action
     *
     * Post-action
     * Process main action
     */
    void clear_label               (Gtk::Label *);
    void pre_set_up_division       (Gtk::Label *, Glib::ustring &);
    void post_set_up_division      (Gtk::Label *, Glib::ustring &);
    void pre_set_up_multiplication (Gtk::Label *, Glib::ustring &);
    void post_set_up_multiplication(Gtk::Label *, Glib::ustring &);
    void pre_set_up_addition       (Gtk::Label *, Glib::ustring &);
    void post_set_up_addition      (Gtk::Label *, Glib::ustring &);
    void pre_set_up_subtraction    (Gtk::Label *, Glib::ustring &);
    void post_set_up_subtraction   (Gtk::Label *, Glib::ustring &);
    void set_up_precision          (Gtk::Label *, Glib::ustring &);
    void set_up_equals             (Gtk::Label *, Glib::ustring &);

    /* Commonly used piece of code function */
    Glib::ustring format_division_dashes(Glib::ustring);
    void trim_operand                   (Glib::ustring);
    void set_operand                    (Gtk::Label *);
    double get_operand                  (Glib::ustring);
    void change_operator(Gtk::Label *, Glib::ustring, const Glib::ustring);
    void toggle_1st_operand_precision_status(Glib::ustring);

    /* Calculation operation */
    bool perform_division      (Gtk::Label *, Glib::ustring &);
    bool perform_multiplication(Glib::ustring &);
    bool perform_addition      (Glib::ustring &);
    bool perform_subtraction   (Glib::ustring &);
};
