#include <memory>

#include "basic-calc.hpp"

static void
on_activate(Glib::RefPtr<Gtk::Application> application)
{
    static std::unique_ptr<Gtk::Window> window;

    if (not window) {
        window = std::make_unique<BasicCalculator>();
        window->set_application(application);
        application->add_window(*window);
    }

    window->present();
}

int
main()
{
    auto application = Gtk::Application::create("io.gitlab.Keyikedalube.basic-calc");

    application->signal_activate().connect(sigc::bind(&on_activate, application));

    return application->run();
}
