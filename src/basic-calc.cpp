#include <iostream>

#include "basic-calc.hpp"
#include "info.hpp"

#include "basic-calc.gresource.c"

BasicCalculator::BasicCalculator()
    : operand                    (0.0)
    , result                     (0.0)
    , is_1st_operand_ready       (false)
    , is_2nd_operand_ready       (false)
    , is_1st_operand_negative    (false)
    , is_1st_operand_in_precision(false)
    , is_2nd_operand_in_precision(false)
    , window_calculator          (nullptr)
    , window_shortcuts           (nullptr)
    , window_header              (nullptr)
    , window_about               (nullptr)
    , window_icon                (nullptr)
{
    /* Calculator window resources */
    try {

        window_calculator = Gtk::Builder::create_from_resource(
                    "/io/gitlab/Keyikedalube/ui/calculator.glade");

        window_shortcuts = Gtk::Builder::create_from_resource(
                    "/io/gitlab/Keyikedalube/ui/shortcuts.glade");

        window_header = Gtk::Builder::create_from_resource(
                    "/io/gitlab/Keyikedalube/ui/headerbar.glade");

        window_about = Gtk::Builder::create_from_resource(
                    "/io/gitlab/Keyikedalube/ui/about.glade");

    } catch (const Gio::ResourceError &exception) {
        std::cerr << exception.what() << "\n";
    } catch (const Glib::MarkupError &exception) {
        std::cerr << exception.what() << "\n";
    } catch (const Gtk::BuilderError &exception) {
        std::cerr << exception.what() << "\n";
    }

    try {
        window_icon = Gdk::Pixbuf::create_from_resource(
                    "/io/gitlab/Keyikedalube/icons/basic-calc.svg");
    } catch (const Gio::ResourceError &exception) {
        std::cerr << exception.what() << "\n";
    } catch (const Gdk::PixbufError &exception) {
        std::cerr << exception.what() << "\n";
    }

    /* Window widget */
    Gtk::Box *box = nullptr;
    window_calculator->get_widget("box", box);
    add(*box);

    /* Display screen */
    Gtk::Label *label = nullptr;
    window_calculator->get_widget("label", label);

    prepare_window();
    prepare_number_buttons(label);
    prepare_action_buttons(label);
    prepare_theme();

    /* Keyboard event */
    signal_key_press_event().connect(
                sigc::bind<Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_key_pressed)
                    , label)
                );

    signal_window_state_event().connect(
                sigc::bind<Gtk::Box *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_window_state_changed)
                    , box)
                );

}

void
BasicCalculator::prepare_window()
{
    /* Custom title bar */
    Gtk::HeaderBar  *header_bar  = nullptr;
    Gtk::MenuButton *menu_button = nullptr;

    window_header->get_widget("header_bar" , header_bar);
    window_header->get_widget("menu_button", menu_button);

    /* Menu items */
    auto menu        = Gio::Menu::create();
    auto menu_action = Gio::SimpleActionGroup::create();

    menu->append("Keyboard Shortcuts", "basic-calc.keyboard_shortcuts");
    menu->append("About"             , "basic-calc.about");

    menu_button->set_menu_model(menu);

    menu_action->add_action("keyboard_shortcuts", sigc::mem_fun(
                                *this, &BasicCalculator::on_action_keyboard_shortcuts));
    menu_action->add_action("about", sigc::mem_fun(
                                *this, &BasicCalculator::on_action_about));

    /* Final steps */
    set_titlebar       (*header_bar);
    //set_default_icon_name("icons/io.gitlab.Keyikedalube.basic-calc-symbolic");
    set_default_icon   (window_icon);
    insert_action_group("basic-calc", menu_action);
}

void
BasicCalculator::prepare_number_buttons(
            Gtk::Label *label)
{
    Gtk::Button *button_zero  = nullptr;
    Gtk::Button *button_one   = nullptr;
    Gtk::Button *button_two   = nullptr;
    Gtk::Button *button_three = nullptr;
    Gtk::Button *button_four  = nullptr;
    Gtk::Button *button_five  = nullptr;
    Gtk::Button *button_six   = nullptr;
    Gtk::Button *button_seven = nullptr;
    Gtk::Button *button_eight = nullptr;
    Gtk::Button *button_nine  = nullptr;
    Gtk::Button *button_point = nullptr;

    window_calculator->get_widget("zero" , button_zero);
    window_calculator->get_widget("one"  , button_one);
    window_calculator->get_widget("two"  , button_two);
    window_calculator->get_widget("three", button_three);
    window_calculator->get_widget("four" , button_four);
    window_calculator->get_widget("five" , button_five);
    window_calculator->get_widget("six"  , button_six);
    window_calculator->get_widget("seven", button_seven);
    window_calculator->get_widget("eight", button_eight);
    window_calculator->get_widget("nine" , button_nine);
    window_calculator->get_widget("point", button_point);

    button_zero->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_zero, label)
                );

    button_one->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_one, label)
                );

    button_two->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_two, label)
                );

    button_three->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_three, label)
                );

    button_four->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_four, label)
                );

    button_five->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_five, label)
                );

    button_six->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_six, label)
                );

    button_seven->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_seven, label)
                );

    button_eight->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_eight, label)
                );

    button_nine->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_nine, label)
                );

    button_point->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_point, label)
                );
}

void
BasicCalculator::prepare_action_buttons(
            Gtk::Label *label)
{
    Gtk::Button *button_clear          = nullptr;
    Gtk::Button *button_division       = nullptr;
    Gtk::Button *button_multiplication = nullptr;
    Gtk::Button *button_addition       = nullptr;
    Gtk::Button *button_subtraction    = nullptr;
    Gtk::Button *button_equals         = nullptr;

    window_calculator->get_widget("clear"         , button_clear);
    window_calculator->get_widget("division"      , button_division);
    window_calculator->get_widget("multiplication", button_multiplication);
    window_calculator->get_widget("addition"      , button_addition);
    window_calculator->get_widget("subtraction"   , button_subtraction);
    window_calculator->get_widget("equals"        , button_equals);

    button_clear->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_clear, label)
                );

    button_division->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_division, label)
                );

    button_multiplication->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_multiplication, label)
                );

    button_addition->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_addition, label)
                );

    button_subtraction->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_subtraction, label)
                );

    button_equals->signal_clicked().connect(
                sigc::bind<Gtk::Button *, Gtk::Label *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_button_clicked)
                    , button_equals, label)
                );
}

void
BasicCalculator::prepare_theme()
{
    auto screen        = Gdk::Screen::get_default();
    auto provider      = Gtk::CssProvider::create();
    auto style_context = get_style_context();

    style_context->add_provider_for_screen(
                screen, provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    provider->load_from_resource("/io/gitlab/Keyikedalube/theme/button.css");
}

bool
BasicCalculator::on_window_state_changed(
        GdkEventWindowState *window_state_event,
            Gtk::Box        *box)
{
    bool is_window_tiled = window_state_event->new_window_state &
            (Gdk::WINDOW_STATE_RIGHT_TILED | Gdk::WINDOW_STATE_LEFT_TILED);
    bool is_window_maximized = window_state_event->new_window_state &
            Gdk::WINDOW_STATE_MAXIMIZED;

    if (is_window_tiled and !is_window_maximized)
        box->set_orientation(Gtk::ORIENTATION_VERTICAL);
    else
        box->set_orientation(Gtk::ORIENTATION_HORIZONTAL);

    return true;
}

void
BasicCalculator::on_action_about()
{
    Gtk::AboutDialog *about_dialog = nullptr;

    window_about->get_widget("about_dialog", about_dialog);

    about_dialog->set_transient_for(*this);
    about_dialog->set_logo_default();
    //about_dialog->set_logo_icon_name("io.gitlab.Keyikedalube.basic-calc");

    Glib::ustring version = "";
    version += Glib::ustring::format(VERSION_MAJOR);
    version.append(".");
    version += Glib::ustring::format(VERSION_MINOR);
    about_dialog->set_version(version);

    about_dialog->set_comments(DESCRIPTION);

    about_dialog->signal_response().connect(
                sigc::bind<Gtk::AboutDialog *>(
                    sigc::mem_fun(*this, &BasicCalculator::on_about_dialog_response)
                    , about_dialog)
                );

    about_dialog->show();
}

void
BasicCalculator::on_about_dialog_response(
                 int         response_id,
            Gtk::AboutDialog *about_dialog)
{
    if (response_id == Gtk::RESPONSE_DELETE_EVENT)
        about_dialog->hide();
}

void
BasicCalculator::on_action_keyboard_shortcuts()
{
    Gtk::ShortcutsWindow *shortcuts_window = nullptr;

    window_shortcuts->get_widget("shortcuts_window", shortcuts_window);

    shortcuts_window->set_transient_for(*this);
    shortcuts_window->set_modal();

    shortcuts_window->present();
}

void
BasicCalculator::on_button_clicked(
            Gtk::Button *button,
            Gtk::Label  *label)
{
    Glib::ustring button_label = button->get_label();
    Glib::ustring label_text   = label->get_text();

    if (button_label == "Clear")
        clear_label(label);
    else if (button_label == "/")
        pre_set_up_division(label, label_text);
    else if (button_label == "*")
        pre_set_up_multiplication(label, label_text);
    else if (button_label == "+")
        pre_set_up_addition(label, label_text);
    else if (button_label == "-")
        pre_set_up_subtraction(label, label_text);
    else if (button_label == ".")
        set_up_precision(label, label_text);
    else if (button_label == "=") {

        if (!label_text.empty() and is_1st_operand_ready)
            set_up_equals(label, label_text);

    } else
        label->set_text(label_text + button_label);
}

bool
BasicCalculator::on_key_pressed(
            GdkEventKey *key,
            Gtk::Label  *label)
{
    Glib::ustring key_string = key->string;
    Glib::ustring label_text = label->get_text();

    if (
            key_string == "1" or key_string == "2" or key_string == "3" or
            key_string == "4" or key_string == "5" or key_string == "6" or
            key_string == "7" or key_string == "8" or key_string == "9" or
            key_string == "0"
            ) {

        label->set_text(label_text + key_string);
        return true;

    } else if (key_string == ".") {

        set_up_precision(label, label_text);
        return true;

    } else if (key->keyval == 65535) {

        /* "delete" keyval is 65535 */
        clear_label(label);
        return true;

    } else {

        char key_char = key_string[0];

        switch (key_char) {
        case '/':
            pre_set_up_division(label, label_text);
            return true;
        case '*':
            pre_set_up_multiplication(label, label_text);
            return true;
        case '+':
            pre_set_up_addition(label, label_text);
            return true;
        case '-':
            pre_set_up_subtraction(label, label_text);
            return true;
        }

    }

    return false;
}

void
BasicCalculator::clear_label(
            Gtk::Label *label)
{
    is_1st_operand_ready        = false;
    /* 2nd operand is always safely false */
    is_1st_operand_negative     = false;
    is_1st_operand_in_precision = false;
    is_2nd_operand_in_precision = false;

    label->set_text("");
}

void
BasicCalculator::pre_set_up_division(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (is_1st_operand_ready)
        set_up_equals(label, label_text);

    if (not label_text.empty()) {
        if (not is_1st_operand_ready)
            post_set_up_division(label, label_text);
        else
            change_operator(label, label_text, "/ ");
    }
}

void
BasicCalculator::post_set_up_division(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (label_text != "+ " and label_text != "- ") {
        /* justify label to center to make the output look beautiful */
        if (label->get_justify() != Gtk::JUSTIFY_CENTER)
            label->set_justify(Gtk::JUSTIFY_CENTER);

        trim_operand(label_text);

        set_operand(label);

        label_text = label->get_text();

        label->set_text(label_text + "⸻\n");

        is_1st_operand_ready = true;
    }
}

void
BasicCalculator::pre_set_up_multiplication(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (is_1st_operand_ready)
        set_up_equals(label, label_text);

    if (not label_text.empty()) {
        if (not is_1st_operand_ready)
            post_set_up_multiplication(label, label_text);
        else
            change_operator(label, label_text, "x ");
    }
}

void
BasicCalculator::post_set_up_multiplication(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (label_text != "+ " and label_text != "- ") {
        /* reset label justification */
        if (label->get_justify() != Gtk::JUSTIFY_RIGHT)
            label->set_justify(Gtk::JUSTIFY_RIGHT);

        trim_operand(label_text);

        set_operand(label);

        label_text = label->get_text();

        label->set_text(label_text + "x ");

        is_1st_operand_ready = true;
    }
}

void
BasicCalculator::pre_set_up_addition(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (is_1st_operand_ready)
        set_up_equals(label, label_text);

    if (not label_text.empty()) {
        if (not is_1st_operand_ready)
            post_set_up_addition(label, label_text);
        else
            change_operator(label, label_text, "+ ");
    } else
        label->set_text("+ ");
}

void
BasicCalculator::post_set_up_addition(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (label_text != "+ ") {
        if (label_text == "- ") {
            label->set_text("+ ");
            is_1st_operand_negative = false;
        } else {
            /* reset label justification */
            if (label->get_justify() != Gtk::JUSTIFY_RIGHT)
                label->set_justify(Gtk::JUSTIFY_RIGHT);

            trim_operand(label_text);

            set_operand(label);

            label_text = label->get_text();

            label->set_text(label_text + "+ ");

            is_1st_operand_ready = true;
        }
    }
}

void
BasicCalculator::pre_set_up_subtraction(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (is_1st_operand_ready)
        set_up_equals(label, label_text);

    if (not label_text.empty()) {
        if (not is_1st_operand_ready)
            post_set_up_subtraction(label, label_text);
        else
            change_operator(label, label_text, "- ");
    } else {
        label->set_text("- ");

        if (not is_1st_operand_ready)
            is_1st_operand_negative = true;
    }
}

void
BasicCalculator::post_set_up_subtraction(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (label_text != "- ") {
        if (label_text == "+ ")
            label->set_text("- ");
        else {
            /* reset label justification */
            if (label->get_justify() != Gtk::JUSTIFY_RIGHT)
                label->set_justify(Gtk::JUSTIFY_RIGHT);

            trim_operand(label_text);

            set_operand(label);

            label_text = label->get_text();

            label->set_text(label_text + "- ");

            is_1st_operand_ready = true;
        }
    }
}

void
BasicCalculator::set_up_precision(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (not is_1st_operand_in_precision) {
        if (not is_1st_operand_ready) {
            label->set_text(label_text + ".");
            is_1st_operand_in_precision = true;
        }
    }

    if (not is_2nd_operand_in_precision) {
        if (is_1st_operand_ready) {
            label->set_text(label_text + ".");
            is_2nd_operand_in_precision = true;
        }
    }
}

void
BasicCalculator::set_up_equals(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    bool is_calculation_done = false;

    /* reset label justification */
    if (label->get_justify() != Gtk::JUSTIFY_RIGHT)
        label->set_justify(Gtk::JUSTIFY_RIGHT);

    /* calculate based on operator */
    is_calculation_done = perform_division(label, label_text);

    if (not is_calculation_done)
        is_calculation_done = perform_multiplication(label_text);

    if (not is_calculation_done)
        is_calculation_done = perform_addition(label_text);

    if (not is_calculation_done)
        is_calculation_done = perform_subtraction(label_text);

    label->set_text(label_text);
}

bool
BasicCalculator::perform_division(
            Gtk::Label   *label,
           Glib::ustring &label_text)
{
    if (label_text.find("⸻") != Glib::ustring::npos) {
        //std::cout << "Division\n";
        Glib::ustring label_text_temp = label_text;
        unsigned long position = label_text_temp.find_last_of("\n");
        position++;
        label_text_temp = label_text_temp.substr(position, label_text_temp.length());

        operand = get_operand(label_text_temp);

        if (is_2nd_operand_ready) {
            result /= operand;
            if (result < 0) {
                result = -result;
                label_text = Glib::ustring::format("- ", result);
            } else {
                label_text = Glib::ustring::format(result);
                is_1st_operand_negative = false;
            }

            toggle_1st_operand_precision_status(label_text);
            is_2nd_operand_in_precision = false;

            is_1st_operand_ready = false;
            is_2nd_operand_ready = false;
        } else {
            if (label->get_justify() != Gtk::JUSTIFY_CENTER)
                label->set_justify(Gtk::JUSTIFY_CENTER);
        }
        return true;
    }
    return false;
}

bool
BasicCalculator::perform_multiplication(
           Glib::ustring &label_text)
{
    if (label_text.find("x ") != Glib::ustring::npos) {
        //std::cout << "Multiplication\n";
        Glib::ustring label_text_temp = label_text;
        unsigned long position = label_text_temp.find_last_of(" ");
        position++;
        label_text_temp = label_text_temp.substr(position, label_text_temp.length());

        operand = get_operand(label_text_temp);

        if (is_2nd_operand_ready) {
            result *= operand;
            if (result < 0) {
                result = -result;
                label_text = Glib::ustring::format("- ", result);
            } else {
                label_text = Glib::ustring::format(result);
                is_1st_operand_negative = false;
            }

            toggle_1st_operand_precision_status(label_text);
            is_2nd_operand_in_precision = false;

            is_1st_operand_ready = false;
            is_2nd_operand_ready = false;
        }
        return true;
    }
    return false;
}

bool
BasicCalculator::perform_addition(
           Glib::ustring &label_text)
{
    if (label_text.find("+ ") != Glib::ustring::npos) {
        //std::cout << "Addition\n";
        Glib::ustring label_text_temp = label_text;
        unsigned long position = label_text_temp.find_last_of(" ");
        position++;
        label_text_temp = label_text_temp.substr(position, label_text_temp.length());

        operand = get_operand(label_text_temp);

        if (is_2nd_operand_ready) {
            result += operand;
            if (result < 0) {
                result = -result;
                label_text = Glib::ustring::format("- ", result);
            } else {
                label_text = Glib::ustring::format(result);
                is_1st_operand_negative = false;
            }

            toggle_1st_operand_precision_status(label_text);
            is_2nd_operand_in_precision = false;

            is_1st_operand_ready = false;
            is_2nd_operand_ready = false;
        }
        return true;
    }
    return false;
}

bool
BasicCalculator::perform_subtraction(
           Glib::ustring &label_text)
{
    if (label_text.find("- ") != Glib::ustring::npos) {
        //std::cout << "Subtraction\n";
        Glib::ustring label_text_temp = label_text;
        unsigned long position = label_text_temp.find_last_of(" ");
        position++;
        label_text_temp = label_text_temp.substr(position, label_text_temp.length());

        operand = get_operand(label_text_temp);

        if (is_2nd_operand_ready) {
            result -= operand;
            if (result < 0) {
                result = -result;
                label_text = Glib::ustring::format("- ", result);
            } else {
                label_text = Glib::ustring::format(result);
                is_1st_operand_negative = false;
            }

            toggle_1st_operand_precision_status(label_text);
            is_2nd_operand_in_precision = false;

            is_1st_operand_ready = false;
            is_2nd_operand_ready = false;
        }
        return true;
    }
    return false;
}

void
BasicCalculator::trim_operand(
           Glib::ustring label_text)
{
    if (label_text.find("- ") != Glib::ustring::npos)
        is_1st_operand_negative = true;

    unsigned long position = label_text.find_first_of(" ");
    position++;
    label_text = label_text.substr(position, label_text.length());

    operand = get_operand(label_text);
}

void
BasicCalculator::set_operand(
            Gtk::Label *label)
{
    if (is_1st_operand_negative) {
        label->set_text("- " + Glib::ustring::format(operand) + "\n");
        result = -operand;
    } else {
        label->set_text(Glib::ustring::format(operand) + "\n");
        result = operand;
    }
}

double
BasicCalculator::get_operand(
           Glib::ustring label_text)
{
    double operand_temp = 0.0;

    try {
        operand_temp = Glib::Ascii::strtod(label_text);
        if (is_1st_operand_ready)
            is_2nd_operand_ready = true;
    } catch (const std::out_of_range &exception) {
        /*
        * in case the user presses "=" or hits enter key
        * while the expression is still incomplete
        *
        * 22/ or 22+ are some examples of incomplete expression
        * since they don't have 2nd operands specified
        */
        if (is_1st_operand_ready)
            is_2nd_operand_ready = false;
    }

    return operand_temp;
}

void
BasicCalculator::change_operator(
            Gtk::Label   *label,
           Glib::ustring label_text,
     const Glib::ustring current_operator)
{
    unsigned long position = label_text.find_last_of("\n");

    /*
     * Division has two newlines to separate two operands; up and below
     * while other operators have just one newline
     *
     * We're now deleting the operator...
     * For +, -, and x we move one step forward
     * For division we've to move one step backward
     */
    if (label_text.find("⸻") != Glib::ustring::npos)
        position--;
    else
        position++;

    label_text = label_text.substr(0, position);
    if (current_operator == "/ ") {
        label->set_justify(Gtk::JUSTIFY_CENTER);

        label->set_text(label_text + "⸻\n");
    } else
        label->set_text(label_text + current_operator);
}

void
BasicCalculator::toggle_1st_operand_precision_status(
           Glib::ustring label_text)
{
    if (label_text.find(".") != Glib::ustring::npos) {
        if (not is_1st_operand_in_precision)
            is_1st_operand_in_precision = true;
    } else {
        if (is_1st_operand_in_precision)
            is_1st_operand_in_precision = false;
    }
}
