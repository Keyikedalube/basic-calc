A very basic and simple visual calculator program implemented using
- GTKMM (GUI toolkit)
- CMake (Build system)

There are two ways you can operate this calculator through either
- Mouse click or
- Keyboard

Goto [wiki page](https://gitlab.com/Keyikedalube/basic-calc/-/wikis/home) for more information
